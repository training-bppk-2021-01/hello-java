# Belajar Anatomi Aplikasi Java

## Compile dan Run ##

1. Command untuk mengecek versi Java SDK

    ```
    javac -version
    ```

2. Command untuk compile source code

    ```
    javac <nama-file>
    ```

3. Command untuk menjalankan aplikasi

    ```
    java <nama-class>
    ```

## Classpath ##

Classpath : tempat / lokasi yang akan dicari oleh Java VM

Di bahasa lain juga ada :
 
* PHP : `include_path`
* VB6 : `COM+ Registry`

Lokasi default yang akan dicari oleh Java VM :

* Folder tempat kita berada
* Lokasi instalasi JDK
* Variabel `CLASSPATH`

Berbagai cara untuk setting `CLASSPATH`

* `CLASSPATH=/folder/tempat/class/file`
* Taruh di environment variable

    * Hanya di satu command prompt window

        * *Nix : `export CLASSPATH=/folder/tempat/class/file`
        * Windows : `set CLASSPATH=\\folder\\tempat\\class\\file`

    * Permanen di seluruh sistem operasi (not recommended)

        * Windows : Advanced Properties
        * *Nix : .bashrc atau /etc/environment, /etc/profile

## Package ##

Struktur folder untuk membuat pengaturan class menjadi rapi. Berguna apabila aplikasi kita besar dan terdiri dari ratusan/ribuan class.

* Deklarasi package harus paling atas

    ```
    package com.muhardin.endy.belajar.java;
    ```

* Nama package : nama domain dibalik. Misal : `com.muhardin.endy.belajar.java`
* Menggunakan class di package lain harus di-import

    ```
    import com.muhardin.endy.belajar.java.entity.Pelanggan;
    ```

* Menjalankan main class harus dengan nama lengkapnya

    ```
    java com.muhardin.endy.belajar.java.Halo endy
    ```

* Pada waktu compile, `javac` akan membuatkan struktur folder sesuai nama package.

## Jar File ##

* Jar file adalah file zip biasa. Bisa dibuat dengan Winzip, WinRAR (pilih tipe ZIP, jangan RAR). 
* Jar file berisi struktur folder package dan class. Perlu diperhatikan : top level dalam zip, harus langsung ke package. Misal : package `com.muhardin.endy.belajar`, maka dalam zip folder pertama yang terlihat adalah `com`
* Untuk menggunakan class di dalam `jar`, caranya sama dengan menggunakan class dalam folder. Cukup arahkan `CLASSPATH` ke file `jar` tersebut. Contoh : 

    ```
    CLASSPATH=aplikasi-halo.jar java com.muhardin.endy.belajar.java.Halo endy
    ```