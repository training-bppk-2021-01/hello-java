package com.muhardin.endy.belajar.java;

import com.muhardin.endy.belajar.java.entity.*;

public class Halo {
    public static void main(String[] x) {
        System.out.println("Halo "+x[0]);

        Pelanggan p = new Pelanggan();
        // p.nama = "Endy"; // sebaiknya tidak akses langsung
        p.setNama("Endy");

        // sebaiknya tidak akses langsung
        // System.out.println("Nama pelanggan : "+p.nama);
        System.out.println("Nama pelanggan : "+p.getNama());
    }
}